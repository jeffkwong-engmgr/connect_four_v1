
var turnCount = 0;
var numRows = 6;
var numCols = 7;

function play (row, col) {

  var color = turnCheck();
  if ( turnCount == 42 ) {
    alert("It's a draw!");
  }
  var filledRow = insertCheckers(col, color);
  //filledRow == -1 means the row is full
  if ( filledRow != -1 ) {
    checkIfConnect4(filledRow, col, color);
  }
}

function insertCheckers (col, color) {
  for ( var emptyRow = numRows-1; emptyRow >= 0; emptyRow-- ) {
    var cellValue = document.getElementById("cell_"+emptyRow+"_"+col).value;
    if (cellValue == "") {
      document.getElementById(emptyRow+"_"+col).style.background = color;
      document.getElementById("cell_"+emptyRow+"_"+col).value = color;
      turnCount++;
      break;
    }
  }
  return emptyRow;
}

function turnCheck () {
  if ( turnCount % 2 == 0 ) {
    return "red";
  } else {
    return "blue";
  }
}

function checkIfConnect4 (row, col, color) {
  if ( checkHorizontal(row, col, color) || checkVertical(row, col, color) || checkDiagonalFSlash(row, col, color) || checkDiagonalBSlash(row, col, color) ) {
    alert("Player " + color + " wins!");
  }
}

function checkHorizontal (row, col, color) {
  //move to the left till edge of board or till color is different
  var x;
  for ( x = col-1; x >= 0; x-- ) {
    var leftColor = document.getElementById("cell_"+row+"_"+x).value;
    if ( leftColor != color ) {
      break;
    }
  }

  //now check connect moving to the right
  var connectCount = 1;
  for ( x+=2; x <= numCols-1; x++ ) {
    //var connectCount = 1;
    var rightColor = document.getElementById("cell_"+row+"_"+x).value;
    if ( rightColor == color ) {
      connectCount++;
      if ( connectCount == 4 ) {
        return true;
      } else {
        continue;
      }
    } else {
      break;
    }
  }
  return false;
}

function checkVertical (row, col, color) {
  //move to the bottom till edge of board or till color is different
  var y;
  for ( y = row+1; y <= numRows-1; y++ ) {
    var bottomColor = document.getElementById("cell_"+y+"_"+col).value;
    if ( bottomColor != color ) {
      break;
    }
  }

  //now check connect moving to the top
  var connectCount = 1;
  for ( y-=2; y >= 0; y-- ) {
    //var connectCount = 1;
    var topColor = document.getElementById("cell_"+y+"_"+col).value;
    if ( topColor == color ) {
      connectCount++;
      if ( connectCount == 4 ) {
        return true;
      } else {
        continue;
      }
    } else {
      break;
    }
  }
  return false;
}

function checkDiagonalFSlash (row, col, color) {
  //move to the lower-left till edge of board or till color is different
  var x,y;
  for ( y = row+1, x= col-1 ; y <= numRows-1 && x >= 0; y++, x-- ) {
    var bottomLeftColor = document.getElementById("cell_"+y+"_"+x).value;
    if ( bottomLeftColor != color ) {
      break;
    }
  }

  //now check connect moving to the upper right
  var connectCount = 1;
  for ( y-=2, x+=2; y >= 0 && x <= numCols-1; y--, x++ ) {
    //var connectCount = 1;
    var topRightColor = document.getElementById("cell_"+y+"_"+x).value;
    if ( topRightColor == color ) {
      connectCount++;
      if ( connectCount == 4 ) {
        return true;
      } else {
        continue;
      }
    } else {
      break;
    }
  }
  return false;
}

function checkDiagonalBSlash (row, col, color) {
  //move to the lower-right till edge of board or till color is different
  var x,y;
  for ( y = row+1, x= col+1 ; y <= numRows-1 && x <= numCols-1; y++, x++ ) {
    var bottomRightColor = document.getElementById("cell_"+y+"_"+x).value;
    if ( bottomRightColor != color ) {
      break;
    }
  }

  //now check connect moving to the upper left
  var connectCount = 1;
  for ( y-=2, x-=2; y >= 0 && x >= 0; y--, x-- ) {
    //var connectCount = 1;
    var topLeftColor = document.getElementById("cell_"+y+"_"+x).value;
    if ( topLeftColor == color ) {
      connectCount++;
      if ( connectCount == 4 ) {
        return true;
      } else {
        continue;
      }
    } else {
      break;
    }
  }
  return false;
}
