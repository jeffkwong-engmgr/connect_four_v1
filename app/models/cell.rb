class Cell < ApplicationRecord
  attr_accessor :x, :y, :color
  def initialize(x, y)
    @x = x
    @y = y
  end
end
