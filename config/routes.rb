Rails.application.routes.draw do

  root 'connect_four#play'
  get 'connect_four/play'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #root 'application#hello'
end
